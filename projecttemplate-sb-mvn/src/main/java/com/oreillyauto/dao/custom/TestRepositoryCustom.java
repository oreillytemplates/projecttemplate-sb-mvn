package com.oreillyauto.dao.custom;

import java.sql.Timestamp;

public interface TestRepositoryCustom {
    public Timestamp getDatabaseTimestamp();
}

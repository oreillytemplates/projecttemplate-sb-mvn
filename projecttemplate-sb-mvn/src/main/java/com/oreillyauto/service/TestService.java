package com.oreillyauto.service;

import java.sql.Timestamp;

public interface TestService {
	public Timestamp getDatabaseTimestamp();
}

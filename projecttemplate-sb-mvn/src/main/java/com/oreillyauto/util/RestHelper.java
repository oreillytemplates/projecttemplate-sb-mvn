package com.oreillyauto.util;

import org.apache.tomcat.util.codec.binary.Base64;

//import org.apache.commons.codec.binary.Base64;

public class RestHelper {
	public static final Base64 base64 = new Base64();
	
    public static boolean isUserAuthenticated(String authString){
        String decodedAuth = "";
        String[] authParts = authString.split("\\s+");
        String authInfo = authParts[1];
        // Decode the data back to original string
        byte[] bytes = null;
        
        try {
            //bytes = new BASE64Decoder().decodeBuffer(authInfo); // sun microsystems
        	bytes = base64.decode(authInfo);                      // apache
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        decodedAuth = new String(bytes);
        System.out.println(decodedAuth);
         
        if ("cGFzc3dvcmQ=".equals(decodedAuth)) {
        	return true;
        }
         
        return false;
    }


    public static void main(String[] args) {
        String encodedString = "password";
        String encodedVersion = new String(base64.encode(encodedString.getBytes()));
        System.out.println("Encoded Version is " + encodedVersion);
        String decodedVersion = new String(base64.decode(encodedVersion.getBytes()));
        System.out.println("Decoded version is "+ decodedVersion);
        //Encoded Version is cGFzc3dvcmQ=
    	//Decoded version is password
    }
    
}
